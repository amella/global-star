﻿Imports System.Xml
Imports System.IO
Imports System.Text
Imports System.Threading

Public Class Service1

    Dim configuration As Configuration = Nothing
    Dim invoice As Invoice = Nothing
    Dim item As Item = Nothing
    Dim routes As Routes = Nothing

    Private WithEvents fsw As FileSystemWatcher

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        Try

            Me.loadConf()

        Catch ex As Exception

            EventLog1.WriteEntry("Error inciando el servicio::" & ex.Message)

            Return

        End Try

        setupWatcher()

    End Sub

    Public Sub New()

        InitializeComponent()

        ' Turn off autologging 
        Me.AutoLog = False
        ' Create a new event source and specify a log name that 
        ' does not exist to create a custom log 
        If Not System.Diagnostics.EventLog.SourceExists("GlobalStarGenerator") Then
            System.Diagnostics.EventLog.CreateEventSource("GlobalStarGenerator", "GlobalStarGeneratorLog")
        End If
        ' Configure the event log instance to use this source name
        EventLog1.Source = "GlobalStarGenerator"

    End Sub


    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
    End Sub

    Private Sub loadConf()

        Try

            Dim conf_route As String = Environment.GetEnvironmentVariable("GLOBAL_STAR_FACT_CONF_FILE", EnvironmentVariableTarget.Machine)

            Dim doc As XmlDocument = New XmlDocument()
            doc.Load(conf_route)

            '1.- Seleccionar FACTURA
            Dim nodeList As XmlNodeList = doc.SelectNodes("//FACTURA")
            Dim node As XmlNode
            Dim childNode As XmlNode

            configuration = New Configuration()
            invoice = New Invoice()
            item = New Item()

            For Each node In nodeList

                For Each childNode In node.ChildNodes

                    Select Case childNode.Name
                        Case "RowState"
                            invoice._RowState = childNode.InnerText
                        Case "NUM_ALMA"
                            invoice._NUM_ALMA = childNode.InnerText
                        Case "CVE_PEDI"
                            invoice._CVE_PEDI = childNode.InnerText
                        Case "ESQUEMA"
                            invoice._ESQUEMA = childNode.InnerText
                        Case "DES_TOT"
                            invoice._DES_TOT = childNode.InnerText
                        Case "DES_FIN"
                            invoice._DES_FIN = childNode.InnerText
                        Case "CVE_VEND"
                            invoice._CVE_VEND = childNode.InnerText

                            If (Not (invoice._CVE_VEND Is Nothing)) And invoice._CVE_VEND <> Constants.EMPTY_STR Then
                                invoice._CVE_VEND = invoice._CVE_VEND.PadLeft(5)
                            End If

                        Case "COM_TOT"
                            invoice._COM_TOT = childNode.InnerText
                        Case "NUM_MONED"
                            invoice._NUM_MONED = childNode.InnerText
                        Case "TIPCAMB"
                            invoice._TIPCAMB = childNode.InnerText
                        Case "MODULO"
                            invoice._MODULO = childNode.InnerText
                        Case "CONDICION"
                            invoice._CONDICION = childNode.InnerText
                        Case "ITEM"

                            For Each childNodeChild In childNode.ChildNodes

                                Select Case childNodeChild.Name
                                    Case "RowState"
                                        item._RowState = childNodeChild.InnerText
                                    Case "DESC1"
                                        item._DESC1 = childNodeChild.InnerText
                                    Case "DESC2"
                                        item._DESC2 = childNodeChild.InnerText
                                    Case "DESC3"
                                        item._DESC3 = childNodeChild.InnerText
                                    Case "IMPU1"
                                        item._IMPU1 = childNodeChild.InnerText
                                    Case "IMPU2"
                                        item._IMPU2 = childNodeChild.InnerText
                                    Case "IMPU3"
                                        item._IMPU3 = childNodeChild.InnerText
                                    Case "IMPU4"
                                        item._IMPU4 = childNodeChild.InnerText
                                    Case "COMI"
                                        item._COMI = childNodeChild.InnerText
                                    Case "NUM_ALM"
                                        item._NUM_ALM = childNodeChild.InnerText
                                    Case "REG_GPOPROD"
                                        item._REG_GPOPROD = childNodeChild.InnerText
                                    Case "TIPO_PROD"
                                        item._TIPO_PROD = childNodeChild.InnerText
                                    Case "TIPO_ELEM"
                                        item._TIPO_ELEM = childNodeChild.InnerText
                                    Case "TIP_CAM"
                                        item._TIP_CAM = childNodeChild.InnerText
                                    Case "UNI_VENTA"
                                        item._UNI_VENTA = childNodeChild.InnerText
                                    Case "IMP1APLA"
                                        item._IMP1APLA = childNodeChild.InnerText
                                    Case "IMP2APLA"
                                        item._IMP2APLA = childNodeChild.InnerText
                                    Case "IMP3APLA"
                                        item._IMP3APLA = childNodeChild.InnerText
                                    Case "IMP4APLA"
                                        item._IMP4APLA = childNodeChild.InnerText                                   
                                End Select
                            Next

                    End Select

                Next

            Next

            invoice._ITEM = item
            configuration._invoice = invoice

            routes = New Routes()

            '2.- Seleccionar Rutas
            nodeList = doc.SelectNodes("//ROUTES")

            For Each node In nodeList
                For Each childNode In node.ChildNodes
                    Select Case childNode.Name
                        Case "IN"
                            routes._in = childNode.InnerText
                        Case "OUT"
                            routes._out = childNode.InnerText
                        Case "ERROR"
                            routes._error = childNode.InnerText
                        Case "PROCESSED"
                            routes._processed = childNode.InnerText
                    End Select
                Next
            Next

            configuration._routes = routes

        Catch ex As Exception


            Throw ex

        End Try


    End Sub

    Sub fsw_Created(ByVal sender As Object, ByVal e As System.IO.FileSystemEventArgs) Handles fsw.Created

        shutDownWatcher()

        Thread.Sleep(10000)

        processDir(configuration._routes._in)

    End Sub

    Sub shutDownWatcher()

        fsw.EnableRaisingEvents = False
        fsw.Dispose()
        fsw = Nothing

    End Sub

    Sub setupWatcher()

        fsw = New FileSystemWatcher(configuration._routes._in, Constants.CSV_EXTENSION)
        fsw.NotifyFilter = 0
        fsw.NotifyFilter = fsw.NotifyFilter Or NotifyFilters.FileName
        fsw.EnableRaisingEvents = True

    End Sub

    Private Sub processDir(ByRef dir As String)

        Try

            Dim dir_info As New DirectoryInfo(dir)
            Dim file_infos As FileInfo() = dir_info.GetFiles()
            Dim files As List(Of String) = New List(Of String)
            Dim fullNames As List(Of String) = New List(Of String)

            For Each f As FileInfo In file_infos
                files.Add(f.Name)
                fullNames.Add(f.FullName)
            Next

            file_infos = Nothing
            dir_info = Nothing

            For index As Integer = 0 To (files.LongCount - 1)

                processFile(files(index), fullNames(index))

                Thread.Sleep(5000)

            Next

        Catch es As Exception

            Throw es

        Finally

            setupWatcher()

        End Try


    End Sub

    Private Sub processFile(ByVal file As String, ByVal fullName As String)

        Dim clientes_unique As New Microsoft.VisualBasic.Collection()
        Dim lines As List(Of String) = New List(Of String)

        Dim clienteExcel As ClienteExcelVO = Nothing

        Dim factura As FacturaExcelVO = New FacturaExcelVO()
        Dim facturaData As FacturaDatosExcelVO = Nothing

        Dim streamReader As StreamReader = Nothing
        Dim line As String = Nothing

        Try

            streamReader = New StreamReader(fullName)
            line = streamReader.ReadLine()

            'Obtener los distintos clientes

            While (line <> Nothing)

                line = line.Trim()

                If line <> Constants.EMPTY_STR Then
                    lines.Add(line)
                End If

                line = streamReader.ReadLine()

            End While


        Catch ex As Exception

        Finally

            If (Not streamReader Is Nothing) Then

                streamReader.Close()
                streamReader = Nothing

            End If

        End Try

        line = Nothing

        Try

            For Each line In lines

                Dim _items As String() = Split(line, Constants.COMMA_CHAR)
                Dim _foo As String = _items(0)

                If Not clientes_unique.Contains(_foo.Trim()) Then

                    clienteExcel = New ClienteExcelVO()
                    clienteExcel._cliente = _foo.Trim().PadLeft(10)

                    clientes_unique.Add(clienteExcel, _foo.Trim())

                End If

            Next

            'obtener las facturas

            Dim _data As FacturaDatosExcelVO = Nothing
            Dim _fact As FacturaExcelVO = Nothing

            For Each line In lines

                Dim _items As String() = Split(line, Constants.COMMA_CHAR)
                Dim _descripcion As String = _items(10)
                Dim _clave_documento As String = _items(1)

                'De entrada tenemos un numero de factura

                If _descripcion = Constants.EMPTY_STR Then

                    'estamos en la misma factura
                    _data = New FacturaDatosExcelVO()

                    _data._obs = _clave_documento
                    _data._articulo = _items(2)
                    _data._cantidad = _items(3)
                    _data._precio_unitario = _items(4)
                    _data._importe = _items(5)
                    _data._ieps = _items(6)
                    _data._subtotal = _items(7)
                    _data._iva = _items(8)
                    _data._total = _items(9)

                    _fact._datos.Add(_data)

                Else
                    'es una nueva fatura
                    _fact = New FacturaExcelVO()

                    _fact._descripcion = _descripcion
                    _data = New FacturaDatosExcelVO()

                    _data._obs = _clave_documento
                    _data._articulo = _items(2)
                    _data._cantidad = _items(3)
                    _data._precio_unitario = _items(4)
                    _data._importe = _items(5)
                    _data._ieps = _items(6)
                    _data._subtotal = _items(7)
                    _data._iva = _items(8)
                    _data._total = _items(9)

                    _fact._datos.Add(_data)

                    Dim _clave As String = _items(0)

                    Dim cliente As ClienteExcelVO = clientes_unique.Item(_clave)
                    cliente._facturas.Add(_fact)

                End If

            Next

        Catch e As Exception

            Dim t As DateTime = DateTime.Now

            Dim g As String = t.Year & t.Month & t.Day & t.Hour & t.Minute & t.Second & t.Millisecond & "_"

            Dim r As String = "move """ & fullName & """ """ & configuration._routes._error & g & file & """"

            Shell("cmd /c " & r, AppWinStyle.Hide)

            Return

        Finally

            lines = Nothing

        End Try

        genFactura(clientes_unique)

        Dim d As DateTime = DateTime.Now

        Dim foo As String = d.Year & d.Month & d.Day & d.Hour & d.Minute & d.Second & d.Millisecond & "_"

        Dim s As String = "move """ & fullName & """ """ & configuration._routes._processed & foo & file & """"

        Shell("cmd /c " & s, AppWinStyle.Hide)

    End Sub

    Private Sub genFactura(ByRef clientes As Microsoft.VisualBasic.Collection)

        Dim d As DateTime = DateTime.Now

        Dim foo As String = d.Year & d.Month & d.Day & d.Hour & d.Minute & d.Second & d.Millisecond & "_"

        For Each cliente As Object In clientes

            For Each f As FacturaExcelVO In cliente._facturas

                Dim writer As XmlTextWriter = New XmlTextWriter(configuration._routes._out & foo & f._descripcion & ".mod", Encoding.UTF8)
                writer.Formatting = Formatting.Indented
                writer.WriteStartDocument(True)

                writer.WriteStartElement("DATAPACKET") 'Inicio de DATAPACKET
                writer.WriteAttributeString("Version", "2.0")

                writer.WriteStartElement("METADATA") 'Inicio de METADATA

                writer.WriteStartElement("FIELDS") 'Inicio de FIELDS

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "CVE_CLPV")
                writer.WriteAttributeString("fieldtype", "string")
                writer.WriteAttributeString("WIDTH", "10")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "NUM_ALMA")
                writer.WriteAttributeString("fieldtype", "i4")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "CVE_PEDI")
                writer.WriteAttributeString("fieldtype", "string")
                writer.WriteAttributeString("WIDTH", "20")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "ESQUEMA")
                writer.WriteAttributeString("fieldtype", "i4")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "DES_TOT")
                writer.WriteAttributeString("fieldtype", "r8")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "DES_FIN")
                writer.WriteAttributeString("fieldtype", "r8")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "CVE_VEND")
                writer.WriteAttributeString("fieldtype", "string")
                writer.WriteAttributeString("WIDTH", "5")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "COM_TOT")
                writer.WriteAttributeString("fieldtype", "r8")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "NUM_MONED")
                writer.WriteAttributeString("fieldtype", "i4")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "TIPCAMB")
                writer.WriteAttributeString("fieldtype", "r8")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "STR_OBS")
                writer.WriteAttributeString("fieldtype", "string")
                writer.WriteAttributeString("WIDTH", "255")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "ENTREGA")
                writer.WriteAttributeString("fieldtype", "string")
                writer.WriteAttributeString("WIDTH", "25")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "SU_REFER")
                writer.WriteAttributeString("fieldtype", "string")
                writer.WriteAttributeString("WIDTH", "20")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "TOT_IND")
                writer.WriteAttributeString("fieldtype", "r8")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "MODULO")
                writer.WriteAttributeString("fieldtype", "string")
                writer.WriteAttributeString("WIDTH", "4")

                writer.WriteEndElement()    'Final de FIELD
                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "CONDICION")
                writer.WriteAttributeString("fieldtype", "string")
                writer.WriteAttributeString("WIDTH", "25")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "dtfield")
                writer.WriteAttributeString("fieldtype", "nested")


                writer.WriteStartElement("FIELDS") 'Inicio de FIELDS

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "CANT")
                writer.WriteAttributeString("fieldtype", "r8")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "CVE_ART")
                writer.WriteAttributeString("fieldtype", "string")
                writer.WriteAttributeString("WIDTH", "20")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "DESC1")
                writer.WriteAttributeString("fieldtype", "r8")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "DESC2")
                writer.WriteAttributeString("fieldtype", "r8")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "DESC3")
                writer.WriteAttributeString("fieldtype", "r8")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "IMPU1")
                writer.WriteAttributeString("fieldtype", "r8")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "IMPU2")
                writer.WriteAttributeString("fieldtype", "r8")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "IMPU3")
                writer.WriteAttributeString("fieldtype", "r8")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "IMPU4")
                writer.WriteAttributeString("fieldtype", "r8")
                writer.WriteEndElement()    'Final de FIELD


                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "COMI")
                writer.WriteAttributeString("fieldtype", "r8")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "PREC")
                writer.WriteAttributeString("fieldtype", "r8")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "NUM_ALM")
                writer.WriteAttributeString("fieldtype", "i4")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "STR_OBS")
                writer.WriteAttributeString("fieldtype", "string")
                writer.WriteAttributeString("WIDTH", "255")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "REG_GPOPROD")
                writer.WriteAttributeString("fieldtype", "i4")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "REG_KITPROD")
                writer.WriteAttributeString("fieldtype", "i4")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "NUM_REG")
                writer.WriteAttributeString("fieldtype", "i4")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "COSTO")
                writer.WriteAttributeString("fieldtype", "r8")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "TIPO_PROD")
                writer.WriteAttributeString("fieldtype", "string")
                writer.WriteAttributeString("WIDTH", "1")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "TIPO_ELEM")
                writer.WriteAttributeString("fieldtype", "string")
                writer.WriteAttributeString("WIDTH", "1")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "MINDIRECTO")
                writer.WriteAttributeString("fieldtype", "r8")

                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "TIP_CAM")
                writer.WriteAttributeString("fieldtype", "r8")

                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "FACT_CONV")
                writer.WriteAttributeString("fieldtype", "r8")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "UNI_VENTA")
                writer.WriteAttributeString("fieldtype", "string")
                writer.WriteAttributeString("WIDTH", "10")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "IMP1APLA")
                writer.WriteAttributeString("fieldtype", "i4")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "IMP2APLA")
                writer.WriteAttributeString("fieldtype", "i4")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "IMP3APLA")
                writer.WriteAttributeString("fieldtype", "i4")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "IMP4APLA")
                writer.WriteAttributeString("fieldtype", "i4")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "PREC_SINREDO")
                writer.WriteAttributeString("fieldtype", "r8")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "COST_SINREDO")
                writer.WriteAttributeString("fieldtype", "r8")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteStartElement("FIELD") 'Inicio de FIELD
                writer.WriteAttributeString("attrname", "LINK_FIELD")
                writer.WriteAttributeString("fieldtype", "ui4")
                writer.WriteAttributeString("linkfield", "true")
                writer.WriteEndElement()    'Final de FIELD

                writer.WriteEndElement()    'Final de FIELDS

                'Cuantos items tiene la factura?
                Dim iCnt As Long = f._datos.LongCount

                Dim paramlog1 As String = ""
                Dim paramlog2 As String = "1 0 4 "

                For cnt As Long = 1 To iCnt

                    paramlog1 = paramlog1 & cnt & " 0 4 "
                    paramlog2 = paramlog2 & "1 1 64 "

                Next

                paramlog1 = RTrim(paramlog1)
                paramlog2 = RTrim(paramlog2)

                writer.WriteStartElement("PARAMS") 'Inicio de PARAMS
                writer.WriteAttributeString("CHANGE_LOG", paramlog1)
                writer.WriteEndElement()    'Final de PARAMS

                writer.WriteEndElement()    'Final de FIELDS
                writer.WriteEndElement()    'Final de FIELD
                writer.WriteStartElement("PARAMS") 'Inicio de PARAMS
                writer.WriteAttributeString("CHANGE_LOG", paramlog2)
                writer.WriteEndElement()    'Final de PARAMS

                writer.WriteEndElement()    'Final de METADATA

                writer.WriteStartElement("ROWDATA") 'Inicio de ROWDATA

                writer.WriteStartElement("ROW") 'Inicio de ROW

                writer.WriteAttributeString("RowState", configuration._invoice._RowState)
                writer.WriteAttributeString("CVE_CLPV", cliente._cliente)
                writer.WriteAttributeString("NUM_ALMA", configuration._invoice._NUM_ALMA)
                writer.WriteAttributeString("CVE_PEDI", configuration._invoice._CVE_PEDI)
                writer.WriteAttributeString("ESQUEMA", configuration._invoice._ESQUEMA)
                writer.WriteAttributeString("DES_TOT", configuration._invoice._DES_TOT)
                writer.WriteAttributeString("DES_FIN", configuration._invoice._DES_FIN)
                writer.WriteAttributeString("CVE_VEND", configuration._invoice._CVE_VEND)
                writer.WriteAttributeString("COM_TOT", configuration._invoice._COM_TOT)
                writer.WriteAttributeString("NUM_MONED", configuration._invoice._NUM_MONED)
                writer.WriteAttributeString("TIPCAMB", configuration._invoice._TIPCAMB)
                writer.WriteAttributeString("STR_OBS", f._descripcion)
                writer.WriteAttributeString("MODULO", configuration._invoice._MODULO)
                writer.WriteAttributeString("CONDICION", configuration._invoice._CONDICION)

                writer.WriteStartElement("dtfield") 'Inicio de dtfield

                Dim iLink As Integer = 1

                For Each w As FacturaDatosExcelVO In f._datos

                    writer.WriteStartElement("ROWdtfield") 'Inicio de ROWdtfield

                    writer.WriteAttributeString("RowState", configuration._invoice._ITEM._RowState)
                    writer.WriteAttributeString("CANT", w._cantidad)
                    writer.WriteAttributeString("CVE_ART", w._articulo)
                    writer.WriteAttributeString("DESC1", configuration._invoice._ITEM._DESC1)
                    writer.WriteAttributeString("DESC2", configuration._invoice._ITEM._DESC2)
                    writer.WriteAttributeString("DESC3", configuration._invoice._ITEM._DESC3)
                    writer.WriteAttributeString("IMPU1", configuration._invoice._ITEM._IMPU1)
                    writer.WriteAttributeString("IMPU2", configuration._invoice._ITEM._IMPU2)
                    writer.WriteAttributeString("IMPU3", configuration._invoice._ITEM._IMPU3)
                    writer.WriteAttributeString("IMPU4", configuration._invoice._ITEM._IMPU4)
                    writer.WriteAttributeString("COMI", configuration._invoice._ITEM._COMI)
                    writer.WriteAttributeString("PREC", w._importe)
                    writer.WriteAttributeString("NUM_ALM", configuration._invoice._ITEM._NUM_ALM)
                    writer.WriteAttributeString("STR_OBS", w._obs)
                    writer.WriteAttributeString("REG_GPOPROD", configuration._invoice._ITEM._REG_GPOPROD)
                    writer.WriteAttributeString("COSTO", w._importe)
                    writer.WriteAttributeString("TIPO_PROD", configuration._invoice._ITEM._TIPO_PROD)
                    writer.WriteAttributeString("TIPO_ELEM", configuration._invoice._ITEM._TIPO_ELEM)
                    writer.WriteAttributeString("TIP_CAM", configuration._invoice._ITEM._TIP_CAM)
                    writer.WriteAttributeString("UNI_VENTA", configuration._invoice._ITEM._UNI_VENTA)
                    writer.WriteAttributeString("IMP1APLA", configuration._invoice._ITEM._IMP1APLA)
                    writer.WriteAttributeString("IMP2APLA", configuration._invoice._ITEM._IMP2APLA)
                    writer.WriteAttributeString("IMP3APLA", configuration._invoice._ITEM._IMP3APLA)
                    writer.WriteAttributeString("IMP4APLA", configuration._invoice._ITEM._IMP4APLA)
                    writer.WriteAttributeString("PREC_SINREDO", w._importe)
                    writer.WriteAttributeString("COST_SINREDO", w._importe)
                    writer.WriteAttributeString("LINK_FIELD", iLink)

                    writer.WriteEndElement()    'Final de ROWdtfield

                    iLink = iLink + 1

                Next

                writer.WriteEndElement()    'Final de dtfield

                writer.WriteEndElement()    'Final de ROW

                writer.WriteEndElement()    'Final de ROWDATA

                writer.WriteEndElement()    'Final de DATAPACKET

                writer.WriteEndDocument()

                writer.Flush()
                writer.Close()

                Thread.Sleep(5000)

            Next


        Next

    End Sub

End Class
